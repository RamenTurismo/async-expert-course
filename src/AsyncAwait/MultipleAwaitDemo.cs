﻿using System.ComponentModel;

namespace AsyncAwait;

[Description("Multiples await demo")]
public class MultipleAwaitDemo
{
    public static async Task StartAsync()
    {
        Task task = SleepAsync();

        DumpThread("Before first await");
        await task;
        DumpThread("Before second await");
        await task;
        DumpThread("End");
    }

    public static async Task SleepAsync()
    {
        DumpThread("Sleep started");
        await Task.Delay(1000);
        DumpThread("Sleep ended");
    }

    public static void DumpThread(string label)
    {
        Console.WriteLine($"[{DateTime.Now:hh:mm:ss.fff}] TID: {Thread.CurrentThread.ManagedThreadId} | {label}");
    }
}