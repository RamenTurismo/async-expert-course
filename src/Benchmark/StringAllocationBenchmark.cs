﻿using BenchmarkDotNet.Attributes;
using System.Text;

namespace AsyncExpert.Benchmark
{
    [MemoryDiagnoser]
    public class StringAllocationBenchmark
    {
        private const int N = 1000;
        private const string BenchmarkString = "lorem ipsum ";

        [Benchmark]
        public string Concat()
        {
            var s = "";

            for (var i = 0; i < N; i++)
            {
                s += BenchmarkString;
            }

            return s;
        }

        [Benchmark]
        public string StringBuilder()
        {
            var s = new StringBuilder();

            for (var i = 0; i < N; i++)
            {
                s.Append(BenchmarkString);
            }

            return s.ToString();
        }

        [Benchmark]
        public string Interpollation()
        {
            var s = "";

            for (var i = 0; i < N; i++)
            {
                s = $"{s}{BenchmarkString}";
            }

            return s;
        }
    }
}