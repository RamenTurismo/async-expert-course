﻿using BenchmarkDotNet.Attributes;

namespace AsyncExpert.Benchmark
{
    [DisassemblyDiagnoser]
    [DryJob] // Doesn't generate file. Allows to just see the output.
    public class DisassemblyBenchmark
    {
        [Benchmark]
        public int WithInterface() => GetAge(_p);

        [Benchmark]
        public int WithConstrainedInterface() => GetAgeWithConstraint(_p);

        private interface IPerson
        {
            int Age { get; }
        }

        private readonly struct Person : IPerson
        {
            public Person(int age)
            {
                Age = age;
            }

            /// <inheritdoc />
            public int Age { get; }
        }

        private readonly Person _p = new Person(41);
        
        private static int GetAge(IPerson person) => person.Age;

        private static int GetAgeWithConstraint<T>(T person) where T : IPerson => person.Age;
    }
}
