﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnostics.Windows.Configs;
using System.IO;

namespace AsyncExpert.Benchmark
{
    /// <summary>
    /// Helps to diagnose potentiel memory leaks.
    /// <para></para>
    /// About Native memory (Managed Heaps / Native Heaps):
    /// https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/fundamentals
    /// https://stackoverflow.com/a/30725210/5794842
    /// </summary>
    [MemoryDiagnoser]
    [NativeMemoryProfiler]
    public class NativeMemoryBenchmark
    {
        [Benchmark]
        public void ReadAndWriteInMemory()
        {
            using var memoryStream = new MemoryStream();
            using var streamWrite = new StreamWriter(memoryStream);
            streamWrite.WriteLine("Tessssssssssssssssst");
        }

        [Benchmark]
        public void ReadAndWriteInMemoryDisposing()
        {
            var memoryStream = new MemoryStream();
            var streamWrite = new StreamWriter(memoryStream);
            streamWrite.WriteLine("Tessssssssssssssssst");
        }
    }
}
