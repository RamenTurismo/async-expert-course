﻿using BenchmarkDotNet.Attributes;
using System.Threading;

namespace AsyncExpert.Benchmark
{
    [ThreadingDiagnoser]
    public class ThreadingBenchmark
    {
        [Benchmark]
        public void CountFromTen()
        {
            var count = 10;
            var locked = new object();

            using var e = new CountdownEvent(count);

            for (int i = 0; i < count; i++)
            {
                ThreadPool.QueueUserWorkItem(
                    m =>
                    {
                        lock (locked)
                        {
                            ((CountdownEvent)m!).Signal();
                        }
                    }, 
                    e);
            }

            e.Wait();
        }
    }
}
