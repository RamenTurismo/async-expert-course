﻿using AsyncExpert.Benchmark.Services;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System.Threading.Tasks;

namespace AsyncExpert.Benchmark
{
    [SimpleJob(RuntimeMoniker.NetCoreApp31)]
    [SimpleJob(RuntimeMoniker.NetCoreApp50)]
    [MarkdownExporter]
    [MemoryDiagnoser]   // Shows the bytes allocated
    public class FakeServiceBenchmark
    {
        private readonly FakeService _service;

        public FakeServiceBenchmark()
        {
            _service = new FakeService();
        }

        /// <summary>
        /// Baseline is the base method that will serve as the ratio base (Adds a column ratio).
        /// </summary>
        [Benchmark(Baseline = true)]
        public async Task CallThree()
        {
            await _service.InstantCallAsync();
        }

        [Benchmark]
        public async Task CallOne()
        {
            await _service.CallDarthVaderAsync();
        }

        [Benchmark]
        public async Task CallTwo()
        {
            await _service.CallAnakinAsync();
        }
    }
}