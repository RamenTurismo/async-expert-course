﻿using System;
using System.ComponentModel;
using System.Threading;

namespace DemoApp
{
    [Description("How the attribute ThreadStatic works.")]
    public static class ThreadStaticDemo
    {
        [ThreadStatic]
        public static int ThreadStatic;

        public static void Start()
        {
            new Thread(() =>
            {
                ThreadStatic = 0;
                Thread.Sleep(1000);
                Console.WriteLine($"First thread: {ThreadStatic}");
            }).Start();


            new Thread(() =>
            {
                ThreadStatic = 1;
                Thread.Sleep(1000);
                Console.WriteLine($"Second thread: {ThreadStatic}");
            }).Start();

            Console.ReadKey();
        }
    }

    [Description("Static initializers are a trap")]
    public class BadDemo
    {
        /// <summary>
        /// Resharper will alert you !
        /// This will have the value of the first thread and then <c>default</c>.
        /// </summary>
        [ThreadStatic]
        public static int ThreadStatic = Thread.CurrentThread.ManagedThreadId;
    }
}
