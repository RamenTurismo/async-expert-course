﻿using System.Threading;

namespace DemoApp;

/// <summary>
/// Thread safe random with usage of the <c>lock</c> keyword.
/// <para></para>
/// Locals 11:47.
/// </summary>
[Description("ThreadSafeRandom")]
public static class ThreadSafeRandom
{
    private static readonly Random _global = new();

    [ThreadStatic]
    private static Random? _local;

    public static int Next()
    {
        if (_local is null)
        {
            int seed;

            lock (_global)
            {
                seed = _global.Next();
            }

            _local = new Random(seed);
        }

        return _local.Next();
    }
}

/// <summary>
/// ThreadLocal is a Lazy per thread.
/// </summary>
public static class ThreadSafeRandomThreadLocal
{
    private static int _seed = 0;
    private static readonly ThreadLocal<Random> _local = new(Initialize);

    private static Random Initialize()
    {
        Interlocked.Increment(ref _seed);
        return new Random(_seed);
    }

    public static int Next() => _local.Value!.Next();
}
