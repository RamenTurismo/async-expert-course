﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace DemoApp
{
    public class QueueProcessor
    {
        private readonly BlockingCollection<string> _queue = new();

        public Task ProcessingAsync(System.Threading.SynchronizationContext? context)
        {
            return Task.Factory.StartNew(async () =>
                    {
                        if (context != null)
                        {
                            System.Threading.SynchronizationContext.SetSynchronizationContext(context);
                        }

                        foreach (var item in _queue.GetConsumingEnumerable())
                        {
                            await ProcessItemAsync(item);
                        }
                    }, TaskCreationOptions.LongRunning)
                .Unwrap();
        }

        public void Queue(string item)
        {
            _queue.Add(item);
        }

        private static async Task ProcessItemAsync(string item)
        {
            var thread = Thread.CurrentThread;
            string threadType = thread.IsThreadPoolThread ? "ThreadPool" : "Not ThreadPool";
            Console.WriteLine($"Before ProcessItemAsync {thread.ManagedThreadId} {threadType}");
            await Task.Delay(item.Length * 100);
            Console.WriteLine($"After ProcessItemAsync {thread.ManagedThreadId} {threadType}");
        }
    }
}