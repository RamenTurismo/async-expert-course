﻿using System;
using System.Threading.Tasks;

namespace DemoApp
{
    public class SynchronizationContextDemo
    {
        public static void Start()
        {
            var context = new SingleThreadSynchronizationContext();
            Task? thread = Task.Run(context.RunOnCurrentThread);

            var queueProcessor = new QueueProcessor();
            Task consumer = queueProcessor.ProcessingAsync(context);

            Task producer = Task.Run(() =>
            {
                while (true)
                {
                    queueProcessor.Queue("Hello world");
                }
            });

            Console.ReadKey();
        }
    }
}