﻿namespace DemoApp;

internal class TaskWhenAll
{
    public static async Task StartAsync()
    {
    }

    private static async Task SumResults()
    {
        Task<int> t1 = Task.FromResult(1);
        Task<int> t2 = Task.FromResult(2);

        int[] results = await Task.WhenAll(t1, t2);

        DemoHelper.WriteInformation($"Sum: {results.Sum()}");
    }

    private static async Task TimeoutPattern(Task task, int miliseconds)
    {
        using CancellationTokenSource? cts = new CancellationTokenSource();

        Task delayTask = Task.Delay(miliseconds, cts.Token);

        // If the resulted task is equals to the delaytask, a timeout has happened.
        if (await Task.WhenAny(task, delayTask) == delayTask)
        {
            throw new OperationCanceledException();
        }
        else
        {
            // Cancel the timer task so it doesn't fire.
            cts.Cancel();
        }

        // Synchronous await.
        await task;
    }
}
