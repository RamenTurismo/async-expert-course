﻿namespace DemoApp;

/// <see href="https://devblogs.microsoft.com/pfxteam/await-anything/" />
[Description("Await anything")]
public static class AwaiterDemo
{
    public static async Task StartAsync()
    {
        Console.WriteLine("Awaiting timespan from 3 seconds");
        await TimeSpan.FromSeconds(3);

        Console.WriteLine("Awaiting CancellationToken");
        using var cs = new CancellationTokenSource(1000);
        await cs.Token;

        Console.WriteLine("Done !");
    }

    public static TaskAwaiter GetAwaiter(this TimeSpan timeSpan)
    {
        return Task.Delay(timeSpan).GetAwaiter();
    }

    public static TaskAwaiter GetAwaiter(this CancellationToken cancellationToken)
    {
        var tcs = new TaskCompletionSource<bool>();
        Task t = tcs.Task;

        if (cancellationToken.IsCancellationRequested)
        {
            tcs.SetResult(true);
        }
        else
        {
            cancellationToken.Register(s => ((TaskCompletionSource<bool>)s!).SetResult(true), tcs);
        }

        return t.GetAwaiter();
    }
}
