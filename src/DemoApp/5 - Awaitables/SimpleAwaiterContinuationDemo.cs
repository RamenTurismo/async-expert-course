﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace DemoApp
{
    [Description("SimpleAwaiterDemo: Continuation")]
    public class SimpleAwaiterContinuationDemo
    {
        public async Task StartAsync()
        {
            string result = await new SimpleAwaitable();

            // This is the continuation line.
            Console.WriteLine($"Result : {result}");
            Console.ReadLine();
        }

        internal class SimpleAwaitable
        {
            public SimpleAwaiter GetAwaiter() => new SimpleAwaiter();
        }

        internal class SimpleAwaiter : INotifyCompletion
        {
            public bool IsCompleted => false;

            /// <inheritdoc />
            public void OnCompleted(Action continuation)
            {
                continuation();
            }

            public string GetResult() => "done!";
        }
    }
}
