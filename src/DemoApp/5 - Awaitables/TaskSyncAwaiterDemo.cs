﻿using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace DemoApp
{
    public static class TaskSyncAwaiterDemo
    {
        public static async Task StartAsync()
        {
            await Process.Start("notepad.exe");
        }

        public static TaskAwaiter<int> GetAwaiter(this Process process)
        {
            var tcs = new TaskCompletionSource<int>(TaskCreationOptions.RunContinuationsAsynchronously);
            process.EnableRaisingEvents = true;
            process.Exited += (sender, _) =>
            {
                if (sender is Process p)
                {
                    tcs.SetResult(p.ExitCode);
                }
            };

            return tcs.Task.GetAwaiter();
        }
    }
}
