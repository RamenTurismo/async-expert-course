﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace DemoApp
{
    [Description("SimpleAwaiterDemo")]
    public class SimpleAwaiterDemo
    {
        public async Task StartAsync()
        {
            var awaitable = new SimpleAwaitable();
            string result = await awaitable;
            Console.WriteLine(result);
            Console.ReadLine();
        }

        internal class SimpleAwaitable
        {
            public SimpleAwaiter GetAwaiter() => new SimpleAwaiter();
        }

        internal class SimpleAwaiter : INotifyCompletion
        {
            public bool IsCompleted => true;

            /// <inheritdoc />
            public void OnCompleted(Action continuation)
            {
                // Never called.
                Console.WriteLine("Completed!");
            }

            public string GetResult() => "done!";
        }
    }
}
