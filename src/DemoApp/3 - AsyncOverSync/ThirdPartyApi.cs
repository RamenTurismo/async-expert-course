﻿using System.Threading.Tasks;

namespace DemoApp
{
    public class ThirdPartyApi
    {
        public static Task<int> GetAsync()
        {
            return Task.FromResult(1);
        }

        public static int Get()
        {
            return 1;
        }
    }
}
