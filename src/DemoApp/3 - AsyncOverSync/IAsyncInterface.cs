﻿using System.Threading;
using System.Threading.Tasks;

namespace DemoApp
{
    public interface IAsyncInterface
    {
        Task<int> GetAsync(CancellationToken cancellationToken);
    }
}
