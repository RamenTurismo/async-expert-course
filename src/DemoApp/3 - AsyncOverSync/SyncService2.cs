﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DemoApp
{
    public class SyncService2 : IAsyncInterface
    {
        /// <inheritdoc />
        public Task<int> GetAsync(CancellationToken cancellationToken)
        {
            try
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    return Task.FromCanceled<int>(cancellationToken);
                }

                return Task.Run(ThirdPartyApi.Get, cancellationToken);
            }
            catch (Exception e)
            {
                return Task.FromException<int>(e);
            }
        }
    }
}