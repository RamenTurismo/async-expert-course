﻿using System.Threading;
using System.Threading.Tasks;

namespace DemoApp
{
    public class AsyncService : IAsyncInterface
    {
        /// <inheritdoc />
        public Task<int> GetAsync(CancellationToken cancellationToken)
        {
            // Async eliding.
            return ThirdPartyApi.GetAsync();
        }
    }
}
