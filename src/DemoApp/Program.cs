﻿namespace DemoApp;

public static class Program
{
    public static Task Main(string[] args)
    {
        return DemoHelper.LookupAsync();
    }
}
