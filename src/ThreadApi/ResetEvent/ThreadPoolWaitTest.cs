namespace ThreadApi.ResetEvent;

public static class ThreadPoolWaitTest
{
    public static void Start()
    {
        var autoResetEvent = new AutoResetEvent(false);

        var callBackInfo = new CallBackInfo();

        callBackInfo.Handle = ThreadPool.RegisterWaitForSingleObject(
            autoResetEvent,
            CallBack,
            callBackInfo,
            TimeSpan.FromSeconds(3),
            true);

        // Long running operation.
        Thread.Sleep(2000);

        // Signals.
        autoResetEvent.Set();

        // Dispose or reuse.
        autoResetEvent.Dispose();
    }

    private static void CallBack(object? state, bool timedOut)
    {
        var callBackInfo = state! as CallBackInfo;
        callBackInfo?.Handle?.Unregister(null);
    }

    public static void ExecuteAfterWait<T>(int sleep, Action<T> action, T arg)
    {
        var autoResetEvent = new AutoResetEvent(false);
        RegisteredWaitHandle? handle = null;

        handle = ThreadPool.RegisterWaitForSingleObject(
            autoResetEvent,
            (state, timeout) =>
            {
                handle!.Unregister(autoResetEvent);
                action(arg);
            },
            null,
            sleep,
            true);
    }
}