﻿namespace ThreadApi;

internal class ThreadPoolManipulation
{
    public ThreadPoolManipulation()
    {
        ThreadPool.QueueUserWorkItem(WaitCallBack);

        ThreadPool.GetAvailableThreads(out int workerThreads, out int completionPortThreads);
        ThreadPool.GetMaxThreads(out int workerThreads2, out int completionPortThreads2);
        ThreadPool.GetMinThreads(out int workerThreads3, out int completionPortThreads3);
    }

    private static void WaitCallBack(object? o)
    {
    }
}