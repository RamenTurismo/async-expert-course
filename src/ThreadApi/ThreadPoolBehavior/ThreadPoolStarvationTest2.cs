﻿namespace ThreadApi.ThreadPoolBehavior;

// Puts the task in the local queue.
public class ThreadPoolStarvationTest2
{
    public static void Start()
    {
        Console.WriteLine(Environment.ProcessorCount);

        ThreadPool.SetMinThreads(8, 8);

        Task.Factory.StartNew(
            Producer,
            TaskCreationOptions.None);
        Console.ReadLine();
    }

    private static void Producer()
    {
        while (true)
        {
            // Creating a new task instead of just calling Process
            // Needed to avoid blocking the loop since we removed the Task.Yield
            Task.Factory.StartNew(Process);

            Thread.Sleep(200);
        }
    }

#pragma warning disable CS1998
    private static async Task Process()
    {
        // Removed the Task.Yield

        var tcs = new TaskCompletionSource<bool>();

        _ = Task.Run(() =>
        {
            Thread.Sleep(1000);
            tcs.SetResult(true);
        });

        tcs.Task.Wait();

        Console.WriteLine("Ended - " + DateTime.Now.ToLongTimeString());
    }
#pragma warning restore CS1998
}