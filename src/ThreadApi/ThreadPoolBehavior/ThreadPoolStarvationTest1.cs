namespace ThreadApi.ThreadPoolBehavior;

// https://labs.criteo.com/2018/10/net-threadpool-starvation-and-how-queuing-makes-it-worse/
public static class ThreadPoolStarvationTest1
{
    /**
        * Note that this code assumes that Environment.ProcessorCount is lower or equal to 8 on your machine. 
        * If it’s bigger, then the threadpool will start with more thread available, 
        * and you need to lower the delay of the Thread.Sleep in Producer() to set the same conditions.
        */
    public static void Start(string[] args)
    {
        Console.WriteLine(Environment.ProcessorCount);

        ThreadPool.SetMinThreads(8, 8);

        Task.Factory.StartNew(
            Producer,
            TaskCreationOptions.None);
        Console.ReadLine();
    }

    private static void Producer()
    {
        while (true)
        {
            _ = Process();

            Thread.Sleep(200);
        }
    }

    private static async Task Process()
    {
        await Task.Yield();

        var tcs = new TaskCompletionSource<bool>();

        _ = Task.Run(() =>
        {
            Thread.Sleep(1000);
            tcs.SetResult(true);
        });

        tcs.Task.Wait();

        Console.WriteLine("Ended - " + DateTime.Now.ToLongTimeString());
    }
}